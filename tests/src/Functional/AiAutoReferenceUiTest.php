<?php

namespace Drupal\Tests\ai_auto_reference\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Contains AI Auto-Reference UI setup functional tests.
 *
 * @group ai_auto_reference
 */
class AiAutoReferenceUiTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'ai',
    'ai_auto_reference',
    'node',
    'taxonomy',
    'user',
    'system',
    'field_ui',
    'views_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to bypass access content.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Nodes for testing the indexing.
   *
   * @var array
   *   An array of nodes for testing.
   */
  protected $nodes = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType([
      'type' => 'article',
      'name' => 'Article',
    ]);

    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'administer content types',
      'access content overview',
      'administer nodes',
      'administer node fields',
      'bypass node access',
      'administer ai',
      'administer ai autoreference',
    ]);

    // Create an entity reference field "Related food" for articles.
    $this->createEntityReferenceField(
      'node',
      'article',
      'field_related_food',
      'Related food',
      'node',
      ['article'],
    );

    $this->createSampleContent();
  }

  /**
   * Helper function to create an entity reference field.
   *
   * @param string $entity_type
   *   The entity type (e.g., 'node').
   * @param string $bundle
   *   The bundle (content type).
   * @param string $field_name
   *   The machine name of the field.
   * @param string $field_label
   *   The human-readable label.
   * @param string $target_type
   *   The entity type being referenced.
   * @param array $target_bundles
   *   Allowed target bundles.
   */
  protected function createEntityReferenceField($entity_type, $bundle, $field_name, $field_label, $target_type, array $target_bundles) {
    $entity_type_manager = \Drupal::entityTypeManager();
    $display_repository = \Drupal::service('entity_display.repository');

    // Create the field storage.
    $field_storage = $entity_type_manager->getStorage('field_storage_config')->create([
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'type' => 'entity_reference',
      'settings' => [
        'target_type' => $target_type,
      ],
      'cardinality' => -1,
      'translatable' => FALSE,
    ]);
    $field_storage->save();

    // Create the field configuration.
    $field = $entity_type_manager->getStorage('field_config')->create([
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'label' => $field_label,
      'settings' => [
        'handler' => 'default',
        'handler_settings' => [
          'target_bundles' => array_combine($target_bundles, $target_bundles),
        ],
      ],
    ]);
    $field->save();

    // Set form display.
    $form_display = $display_repository->getFormDisplay($entity_type, $bundle, 'default');
    $form_display->setComponent($field_name, [
      'type' => 'entity_reference_autocomplete',
    ])->save();

    // Set view display.
    $view_display = $display_repository->getViewDisplay($entity_type, $bundle, 'default');
    $view_display->setComponent($field_name, [
      'type' => 'entity_reference_label',
    ])->save();
  }

  /**
   * Create sample content to check index.
   */
  public function createSampleContent(): void {
    $this->nodes[] = $this->drupalCreateNode([
      'type' => 'article',
      'title' => 'Chocolate Cake',
      'field_body' => [
        'value' => 'A delicious chocolate dessert made with cocoa powder and dark chocolate.',
        'format' => 'plain_text',
      ],
    ]);
    $this->nodes[] = $this->drupalCreateNode([
      'type' => 'article',
      'title' => 'Strawberry Cheese Cake',
      'field_body' => [
        'value' => 'A sweet cheese based dessert make with strawberries on a pie-like crust.',
        'format' => 'plain_text',
      ],
      'status' => 0,
    ]);
    $this->nodes[] = $this->drupalCreateNode([
      'type' => 'article',
      'title' => 'Vanilla Ice Cream',
      'field_body' => [
        'value' => 'A creamy vanilla dessert made with milk, cream, and vanilla extract.',
        'format' => 'plain_text',
      ],
    ]);
    $this->nodes[] = $this->drupalCreateNode([
      'type' => 'article',
      'title' => 'Tomato Soup',
      'field_body' => [
        'value' => 'A warm starter made with fresh tomatoes, garlic, and basil.',
        'format' => 'plain_text',
      ],
    ]);
  }

  /**
   * Test the access.
   */
  public function testSettingsPageAccess() {
    $this->drupalGet('admin/config/content/ai-autoreference-settings');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('admin/structure/types/manage/article/ai-autoreference');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test the settings.
   */
  public function testNodeSettings() {
    $this->drupalLogin($this->adminUser);

    // Check that no button exists yet.
    $this->drupalGet('node/1/edit');
    $this->assertSession()->buttonNotExists('Generate references with AI');

    // Create a prompt.
    $this->drupalGet('admin/config/content/ai-autoreference-settings/prompts');
    $this->clickLink('Add Prompt');
    $this->submitForm([
      'label' => 'My prompt',
      'id' => 'my_prompt',
      'prompt' => 'This is my prompt',
    ], 'Save');
    $this->assertSession()->pageTextContains('The prompt must contain both {CONTENTS} and {POSSIBLE_RESULTS}.');
    $this->submitForm([
      'label' => 'Test create prompt',
      'id' => 'my_prompt',
      'prompt' => 'This is my prompt with {CONTENTS} and {POSSIBLE_RESULTS}',
    ], 'Save');

    // Check that it is now saved.
    $this->assertSession()->elementTextContains('css', 'table', 'AI Auto-reference Prompt');
    $this->assertSession()->elementTextContains('css', 'table', 'Test create prompt');

    // Create a new auto-reference.
    $this->drupalGet('admin/structure/types/manage/article/ai-autoreference');
    $this->submitForm([
      'table[add_new_autoreference][field_name]' => 'field_related_food',
      'table[add_new_autoreference][view_mode]' => 'teaser',
      // This validates that the default prompts were created on install.
      'table[add_new_autoreference][prompt]' => 'default_multiple_prompt',
    ], 'Add autoreference');

    // Now edit it.
    $this->clickLink('Edit');

    // Re-save.
    $this->submitForm([], 'Save settings');

    // Delete it.
    $this->drupalGet('admin/structure/types/manage/article/ai-autoreference/field_related_food/delete');
    $this->submitForm([], 'Confirm');

    // Add it again.
    $this->submitForm([
      'table[add_new_autoreference][field_name]' => 'field_related_food',
      'table[add_new_autoreference][view_mode]' => 'teaser',
      'table[add_new_autoreference][prompt]' => 'my_prompt',
    ], 'Add autoreference');

    // Edit a node.
    $this->drupalGet('node/1/edit');
    $this->assertSession()->buttonExists('Generate references with AI');

    // Pretend we have generated references.
    $url = Url::fromRoute('entity.node.edit_form', ['node' => 1], [
      'query' => [
        'field_related_food[h]' => '1,2',
        'field_related_food[m]' => '3',
        'auto-apply' => '',
      ],
    ]);
    $this->drupalGet($url);
    // High results.
    $this->assertSession()->elementTextContains(
      'css',
      '.js-form-item-container-field-related-food-h-1 label',
      'Chocolate Cake',
    );
    $this->assertSession()->elementTextContains(
      'css',
      '.js-form-item-container-field-related-food-h-2 label',
      'Strawberry Cheese Cake',
    );
    // Medium results.
    $this->assertSession()->elementTextContains(
      'css',
      '.js-form-item-container-field-related-food-m-3 label',
      'Vanilla Ice Cream',
    );
  }

  /**
   * Tests that the allowed values provides expected results.
   */
  public function testAiGeneratorAllowedValues(): void {
    $this->drupalCreateContentType([
      'type' => 'recipe',
      'name' => 'Recipe',
    ]);
    $this->createEntityReferenceField(
      'node',
      'article',
      'field_related_multiple',
      'Related multiple types',
      'node',
      ['article', 'recipe'],
    );
    $this->nodes[] = $this->drupalCreateNode([
      'type' => 'recipe',
      'title' => 'Chocolate Cake Recipe',
      'field_body' => [
        'value' => '10 cups of sugar',
        'format' => 'plain_text',
      ],
    ]);
    $article = $this->drupalCreateNode([
      'type' => 'article',
      'title' => 'All about chocolate cakes',
      'field_body' => [
        'value' => 'Here we reference multiple types',
        'format' => 'plain_text',
      ],
    ]);

    /** @var \Drupal\ai_auto_reference\AiReferenceGenerator */
    $generator = \Drupal::service('ai_auto_reference.ai_references_generator');

    // Field with a single target bundle.
    $allowed_values = $generator->getFieldAllowedValues($article, 'field_related_food');
    $this->assertSame([
      // These are all articles.
      1 => 'Chocolate Cake',
      3 => 'Vanilla Ice Cream',
      4 => 'Tomato Soup',
    ], $allowed_values);

    // Field with multiple target bundles.
    $allowed_values = $generator->getFieldAllowedValues($article, 'field_related_multiple');
    $this->assertSame([
      // These are articles.
      1 => 'Chocolate Cake',
      3 => 'Vanilla Ice Cream',
      4 => 'Tomato Soup',
      // This one is a recipe.
      5 => 'Chocolate Cake Recipe',
    ], $allowed_values);
  }

}
