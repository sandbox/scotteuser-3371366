# AI Auto-reference

This module is intended to use AI to automatically suggest relationships
to other content within the same website. Typically this would be automatically
tagging/categorising content. By default this module ships with integration with
chatgpt.

- For a full description of the module, visit the
  [project page](https://www.drupal.org/sandbox/scotteuser/3371366).

- To submit bug reports or feature suggestions, or track changes
  [issue queue](https://www.drupal.org/project/issues/3371366).


## Table of contents

- Features
- Requirements
- Installation
- Configuration
- Maintainers


## Features

Set up automatic references for any entity reference field
(whether it be to taxonomy terms, or other nodes) Automatically first shortens
the content if needed in order to fit within token limits of the API Adjust
your token limit (eg, if you have been accepted for a higher token limit)
Decide whether you wish to review suggestions from the AI before saving the
relationships to your content, or whether to auto-apply.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Go to /admin/config/content/ai-autoreference-settings and set
  configuration settings.


## Post-Installation

Add your API key to the configuration page or to your settings.local.php for
example (at least latter recommended to not version control your API key, but
consider using eg the 'key' module).
Determine per node bundle which entity reference fields you would like to have
the AI suggest results for.


## Maintainers

- Scott Euser - [scott_euser](https://www.drupal.org/u/scott_euser)
- Andrii Sakhaniuk - [nnevill](https://www.drupal.org/u/nnevill)
