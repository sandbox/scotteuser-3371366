<?php

namespace Drupal\ai_auto_reference;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Provides a listing of AI Auto-reference Prompts.
 */
class AiAutoReferencePromptListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('AI Auto-reference Prompt');
    $header['id'] = $this->t('Machine name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    // Add "Add AI Auto-reference Prompt" button.
    $build['actions'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['entity-add-link']],
      'add' => [
        '#type' => 'link',
        '#title' => $this->t('Add Prompt'),
        '#url' => Url::fromRoute('entity.ai_auto_reference_prompt.add_form'),
        '#attributes' => ['class' => ['button', 'button--primary']],
      ],
    ];

    return $build;
  }

}
