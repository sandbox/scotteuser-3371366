<?php

namespace Drupal\ai_auto_reference;

use Drupal\ai\AiProviderPluginManager;
use Drupal\ai\OperationType\Chat\ChatInput;
use Drupal\ai\OperationType\Chat\ChatMessage;
use Drupal\ai\Utility\TextChunkerInterface;
use Drupal\ai\Utility\TokenizerInterface;
use Drupal\ai_auto_reference\Entity\AiAutoReferencePromptInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Theme\ThemeInitializationInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use League\HTMLToMarkdown\HtmlConverter;

/**
 * Class for the AI references generation.
 */
class AiReferenceGenerator {

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Constructs a new AiReferencesGenerator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config interface.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $channelFactory
   *   The logger channel factory.
   * @param \Drupal\Core\Theme\ThemeInitializationInterface $themeInitialization
   *   The theme init.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   The theme manager.
   * @param \Drupal\ai\AiProviderPluginManager $aiProviderManager
   *   The AI provider plugin manager.
   * @param \Drupal\ai\Utility\TokenizerInterface $tokenizer
   *   The tokenizer.
   * @param \Drupal\ai\Utility\TextChunkerInterface $textChunker
   *   The text chunker.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected RendererInterface $renderer,
    protected ConfigFactoryInterface $config,
    protected AccountInterface $currentUser,
    protected LoggerChannelFactoryInterface $channelFactory,
    protected ThemeInitializationInterface $themeInitialization,
    protected ThemeManagerInterface $themeManager,
    protected AiProviderPluginManager $aiProviderManager,
    protected TokenizerInterface $tokenizer,
    protected TextChunkerInterface $textChunker,
  ) {
    $this->logger = $this->channelFactory->get('ai_auto_reference');
  }

  /**
   * Gets AI auto-reference configuration per bundle.
   *
   * @param string $bundle
   *   Node bundle machine name.
   *
   * @return array
   *   Array with configurations.
   */
  public function getBundleAiReferencesConfiguration($bundle) {
    /** @var \Drupal\Core\Entity\Display\EntityDisplayInterface $form_display_entity */
    $form_display_entity = $this->entityTypeManager
      ->getStorage('entity_form_display')
      ->load("node.{$bundle}.default");
    $settings = $form_display_entity->getThirdPartySettings('ai_auto_reference');
    if (empty($settings)) {
      return $settings;
    }

    $ai_references = [];
    foreach ($settings as $field_name => $setting) {
      // Add AI autogenerate button only if at least one field is configured.
      if (!empty($setting['view_mode']) && !empty($setting['prompt'])) {
        $ai_references[] = [
          'field_name' => $field_name,
          'view_mode' => $setting['view_mode'],
          'prompt' => $setting['prompt'],
        ];
      }
    }

    return $ai_references;
  }

  /**
   * Gets AI auto-reference suggestions.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node object.
   * @param string $field_name
   *   Field machine name.
   * @param string $view_mode
   *   Entity view mode.
   * @param string $prompt
   *   The prompt machine name.
   *
   * @return array
   *   Array with suggestions.
   */
  public function getAiSuggestions(
    NodeInterface $node,
    string $field_name,
    string $view_mode,
    string $prompt,
  ) {
    $config = $this->config->get('ai_auto_reference.settings');
    $chat_model_id = $this->aiProviderManager->getModelNameFromSimpleOption($config->get('provider'));
    $chat_model_id = $chat_model_id ?: 'gpt-3.5';
    $this->textChunker->setModel($chat_model_id);

    // Node edit link to use in logs.
    $url = Url::fromRoute('entity.node.edit_form', [
      'node' => $node->id(),
    ]);
    $node_edit_link = Link::fromTextAndUrl($node->label(), $url)->toString();
    try {
      // Might be 4000, 8000, 32000 depending on version used and
      // beta / waiting list access approval.
      $token_limit = $config->get('token_limit');

      $possible_results = $this->getFieldAllowedValues($node, $field_name);
      $imploded_possible_results = implode('|', $possible_results);

      // Getting current (in most cases – admin) theme.
      $active_theme = $this->themeManager->getActiveTheme();
      // Getting default theme.
      $default_theme = $this->config->get('system.theme')->get('default');

      // We need to be sure we render content in default theme.
      $this->themeManager->setActiveTheme($this->themeInitialization->initTheme($default_theme));
      $render_controller = $this->entityTypeManager->getViewBuilder($node->getEntityTypeId());

      // Get the rendered contents of the node.
      $render_output = $render_controller->view($node, $view_mode);
      $rendered_output = $this->renderer->renderInIsolation($render_output);
      $converter = new HtmlConverter();
      $contents = $converter->convert($rendered_output);
      $contents = preg_replace('/\s+/S', " ", $contents);

      // Switching back to admin theme.
      $this->themeManager->setActiveTheme($active_theme);

      // Build the prompt to send.
      $prompt = $this->entityTypeManager->getStorage('ai_auto_reference_prompt')->load($prompt);
      if ($prompt instanceof AiAutoReferencePromptInterface) {
        // Retrieve prompt from configuration.
        $prompt_text = $prompt->getPrompt();
      }
      else {
        // Fallback in case the target prompt was deleted.
        $prompt_text = "For the contents within the block:\n```{CONTENTS}\n```\n\n";
        $prompt_text .= "Which two to four of the following `|` separated options are highly relevant and moderately relevant?\n```{POSSIBLE_RESULTS}\n```\n\n";
        $prompt_text .= "Return selections from within the square brackets only and as a valid RFC8259 compliant JSON array within two array keys `highly` and `moderately` without deviation.\n\n";
        $prompt_text .= "Example:\r\n{\r\n    \"highly\": [\r\n        \"First suggestion\",\r\n        \"Second suggestion\"\r\n    ],\r\n    \"moderately\": [\r\n        \"Third suggestion\",\r\n        \"Fourth suggestion\"\r\n    ]\r\n}";
      }

      // If the prompt is longer than the limit, get a summary of the contents.
      $prompt_length = $this->tokenizer->countTokens($prompt_text);
      $possible_results_length = $this->tokenizer->countTokens($imploded_possible_results);
      $contents_length = $this->tokenizer->countTokens($contents);
      if ($prompt_length + $possible_results_length > $token_limit) {
        $this->logger->error('The prompt and possible results alone use more tokens than the token limit for %field_name for %node_edit. The job cannot be completed', [
          '%field_name' => $field_name,
          '%node_edit' => $node_edit_link,
        ]);
      }
      elseif ($prompt_length + $possible_results_length + $contents_length > $token_limit) {
        // Make an API call to get a summary of the content.
        $available_contents_tokens = $token_limit - $prompt_length - $possible_results_length;
        // @todo allow configuration of the text shortening prompt.
        $summary_prompt = 'Shorten this text into a maximum of ' . $available_contents_tokens . ' tokens and minimum of ' . ($available_contents_tokens - 1000) . ' tokens: ';
        $summary_prompt_length = $this->tokenizer->countTokens($summary_prompt);
        if ($contents_length + $summary_prompt_length > $token_limit) {

          // If even this without the possible results is too long, we
          // just crop it - no choice.
          $max_size = (int) ($token_limit - $summary_prompt_length);
          $content_chunks = $this->textChunker->chunkText($contents, $max_size, 0);
          $contents = reset($content_chunks);
        }

        // Add contents (potentially cropped) to the prompt.
        $summary_prompt .= $contents;

        // Get the summary back.
        $response = $this->aiApiCall($config, $summary_prompt);
        if (!empty($response)) {
          // Replace the contents with summarized contents.
          $contents = $response;
        }
      }

      // Run replacements in the prompt.
      $prompt_text = str_replace('{POSSIBLE_RESULTS}', $imploded_possible_results, $prompt_text);
      $prompt_text = str_replace('{CONTENTS}', $contents, $prompt_text);

      // Get the array of 'highly' and 'moderately' related results back.
      if ($answer = $this->aiApiCall($config, $prompt_text)) {
        $answer = Json::decode($answer);
        $suggestions['h'] = !empty($answer['highly']) ? array_keys(array_intersect($possible_results, $answer['highly'])) : [];
        $suggestions['m'] = !empty($answer['moderately']) ? array_keys(array_intersect($possible_results, $answer['moderately'])) : [];
        return $suggestions;
      }

    }
    catch (\Exception $exception) {
      $this->logger->error('AI auto-reference generation failed for %field_name for %node_edit. Error message: %error_message', [
        '%field_name' => $field_name,
        '%node_edit' => $node_edit_link,
        '%error_message' => $exception->getMessage(),
      ]);
    }
    // Fallback to empty array.
    return [];
  }

  /**
   * Gets possible values for auto-reference.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node object.
   * @param string $field_name
   *   Field machine name.
   *
   * @return array
   *   Array with possible values.
   */
  public function getFieldAllowedValues(NodeInterface $node, $field_name): array {
    if ($field_definition = $node->getFieldDefinition($field_name)) {
      $options = $field_definition
        ->getFieldStorageDefinition()
        ->getOptionsProvider('target_id', $node)
        ->getSettableOptions($this->currentUser);
      // Exclude existing values.
      foreach ($node->get($field_name)->getValue() as $existing_value) {
        if (!empty($existing_value['target_id'])) {
          unset($options[$existing_value['target_id']]);
        }
      }

      // Get settable options can return options group by bundle type if there
      // are multiple target bundles.
      if ($options) {
        $first_option = reset($options);
        if (is_array($first_option)) {
          $merged_options = [];
          foreach ($options as $option_group) {
            $merged_options += $option_group;
          }
          $options = $merged_options;
        }
      }

      // Do not suggest a reference to the current node.
      if (isset($options[$node->id()])) {
        unset($options[$node->id()]);
      }

      return $options;
    }
    return [];
  }

  /**
   * Performs AI API call.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   AI auto-reference config.
   * @param string $prompt_text
   *   AI prompt.
   *
   * @return string
   *   AI answer.
   */
  public function aiApiCall(ImmutableConfig $config, string $prompt_text): string {
    $provider = $config->get('provider');
    $ai_provider = $this->aiProviderManager->loadProviderFromSimpleOption($provider);
    $ai_model = $this->aiProviderManager->getModelNameFromSimpleOption($provider);
    $messages = new ChatInput([
      new ChatMessage('user', $prompt_text),
    ]);
    $message = $ai_provider->chat($messages, $ai_model)->getNormalized()->getText();
    $message = str_replace(['```json', '```'], '', $message);
    return trim($message) ?? '';
  }

}
